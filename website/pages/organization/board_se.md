# Styrelsen

LiTHe kods styrelse består av ett antal styrelseledamöter som väljs på
vårmötet. Styrelseledamöter tar beslut om föreningens verksamhet och förbereder
stormöten. Bland styrelseledamöter finns rollerna ordförande, vice ordförande,
kassör, verksamhetsledare, som tar på sig lite extra ansvar. Utöver
styrelseledamöter så finns det en revisor som granskar årets verksamhet.

LiTHe kods styrelse bedriver en tävlingsplats för
[NCPC](https://nordic.icpc.io/ncpc2024/), att skicka vidare tävlande till
[NWERC](https://2024.nwerc.eu/), Advent of Code, evenemang med företag, och har
en lokal i kårallen de får pyssla med.

För kompletterande information kan man läsa
[föreningens stadgar](https://gitlab.com/lithekod/stadgar/-/raw/master/stadgar.pdf).

## Ordförande

Ordförande är föreningens högsta höns. Det är ordförandes ansvar att
representera föreningen och är föreningens kontaktperson. Ordförande leder
organisationen och ser till att föreningens verksamhet sker i enlighet med
föreningens syfte och stadgar.

## Vice ordförande

Som vice ordförande är man lite av en jack-of-all-trades för föreningen. Utöver
det så är vice ordförande en ersättare för ordförande, om ordförande får
förhinder till möten.

## Kassör

Kassören ansvarar för föreningens ekonomi. Bland annat avser det att upprätta en
budget, att bokföra föreningens ekonomi, betala fakturor, kontrollera
föreningens tillgångar och vid varje stormöte redovisera föreningens ekonomiska
ställning.

## Verksamhetsledare

Verksamhetsledarens ansvar är att organisera och informera om aktiviteter.
Många aktiviteter hanteras av gruppledare, det är då verksamhetsledarens ansvar
att se till att gruppledare har de resurser som behövs för att genomföra
aktiviteten och att vara kontaktperson för gruppledare.

# Gruppledare

Föreningen ägnar sig åt diverse olika aktiviteter. För att underlätta
organisationen av respektive aktivitet så utser föreningen olika gruppledare,
vars roller är att organisera och leda aktiviteter.

## LiU Game Jam

LiU Game Jam är en av grundpelarna i föreningens verksamhet. Som gruppledare
för LiU Game Jam är det din uppgift att organisera och leda game jams.
Vanligtvis så hålls tre stycken game jams per år; Spring Game Jam, Global Game
Jam och Fall Game Jam. Att anordna ett Game Jam är inte en liten uppgift, till
din hjälp har du organizers som hjälper till att organisera och genomföra game
jamet.
