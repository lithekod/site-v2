# The Board

The board of LiTHe kod consists of a number of board members elected at the
spring meeting. Board members make decisions about the association's activities
and prepare general meetings. Among the board members, there are roles such as
chair, vice chair, treasurer, and activity leader, who take on a
bit of extra responsibility. In addition to the board members, there is an
auditor who reviews the year's activities.

LiTHe kod's board manages a competition site for
[NCPC](https://nordic.icpc.io/ncpc2024/), sends competitors to
[NWERC](https://2024.nwerc.eu/), participates in Advent of Code, hosts events
with companies, and has a space in Kårallen where they can work on projects.

For additional information, you can read the [association's
bylaws](https://gitlab.com/lithekod/stadgar/-/raw/master/stadgar.pdf).

## Chair

The chair is the head of the association. It is the chair's responsibility to
represent the association and serve as its contact person. The chair leads the
organization and ensures that the association's activities are conducted in
accordance with its purpose and bylaws.

## Vice Chair

As vice chair, you are somewhat of a jack-of-all-trades for the association.
Additionally, the vice chair serves as a substitute for the chair in case the
chair is unable to attend meetings.

## Treasurer

The treasurer is responsible for the association's finances. This includes
preparing a budget, keeping the association's accounts, paying invoices,
monitoring the association's assets, and presenting the association's financial
status at each general meeting.

## Activity Leader

The activity leader's responsibility is to organize and inform about
activities. Many activities are managed by group leaders, and it is the
activity leader's responsibility to ensure that group leaders have the
resources they need to carry out the activities and to act as the contact
person for the group leaders.

# Group Leaders

The association engages in various activities. To facilitate the organization
of each activity, the association appoints different group leaders whose roles
are to organize and lead activities.

## LiU Game Jam

LiU Game Jam is one of the cornerstones of the association's activities. As the
group leader for LiU Game Jam, it is your task to organize and lead game jams.
Usually, three game jams are held per year: Spring Game Jam, Global Game Jam,
and Fall Game Jam. Organizing a game jam is no small task, and you have
organizers to assist you in planning and executing the event.
