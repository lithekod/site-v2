# Organization

Here you can find documents and information tied to the organization.

## [Fall and spring meetings](/meetings/)

## [Gitlab](https://gitlab.com/lithekod)

## [Bylaws](https://gitlab.com/lithekod/stadgar/-/raw/master/stadgar.pdf)

## [Protocols](https://gitlab.com/lithekod/stormoten)

## [Board](/organization/board)
