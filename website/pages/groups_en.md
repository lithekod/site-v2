# LiTHe kod's subgroups

LiTHe kod is split into subgroups! Here you can read more about which subgroups
exist and what they're up to.

Subgroup leaders are elected on a yearly basis on
[vårmötet](/organization/meetings/), together with the rest of the board.

- [**LiU Game Jam**](/gamejam/) have organized game jams in Linköping since
  2013\. During a game jam you get to create a game from scratch during a
  weekend, either solo or in a group. LiU Game Jam normally organizes three game
  jams every year: Fall Game Jam during the fall, Global Game Jam during the
  winter and Spring Game Jam in the spring.
- The [competitive programming group](/competitions/) organizes different events
  that are all related to competitive programming and problem solving. In the
  fall we organize a site for [NCPC](/competitions/ncpc/), where one or two
  teams can progress to [NWERC](https://www.nwerc.eu/). For those of you who
  prefer to solve programming puzzles at your own pace we have [Advent of
  Code](/competitions/aoc/) (an advent calendar with programming puzzles, every
  December) and [IMPA](/competitions/impa/) (a programming competition for
  university students during the entirety of the term).
- The [hardware group](/hardware/) is for you who is interested in programmable
  hardware and other hardware-adjacent programming. This is a pretty new group
  and don't have many planned events, so if you're interested or just want to
  learn more, feel free to get in touch!
- The meetup group makes sure that fikavagnen is loaded with cookies, coffe and
  tea for our meetups every Tuesday.

Additionally we have a few internal groups. These don't have any public events,
but are still vital parts of LiTHe kod.

- The web group takes care of LiTHe kod's website (which you are reading right
  now!), so that the board and subgroups can inform about their activities. The
  web group also handles our server at Lysator, and the member database.
- The room group (better name needed) takes care of our very own cozy room in
  Kårallen! <!--The room is available for subgroups and members who want to have
  somewhere to run their own project. More information can be found on [our
  project site](/organization/projects/) or by contacting our room subgroup
  leader.-->
