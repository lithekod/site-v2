# Hårdvarugruppen

Välkommen till LiTHe kods hårdvarugrupp! Vi är en undergrupp till LiTHe kod som
är intresserade av hårdvara och annan hårdvarunära programmering, som till
exempel programmerbar hårdvara, mikrokontroller och inbyggda system.

Den 22 februari kommer vi ha vårt första event: en [FPGA-introduktionskväll på
Goto10](fpga-evening)!

Eftersom vi är en ganska ny grupp har vi just nu ingen regelbunden verksamhet.
Men det betyder bara att det finns mer att säga till om! Om du är intresserad av
vad vi håller på med, eller bara vill lära dig mer, får du jättegärna höra av
dig till hårdvarugruppen. Vi nås enklast antingen i kanalen `#hårdvara` i [LiTHe
kods Discordserver](https://discord.gg/UG5YYsN) eller på LiTHe kods meetups
varje tisdag klockan 17 i Café Java.

Här är några saker vi funderar på att göra:

- En egen logotyp (som ser bra ut på en 128x32 SSD1306 OLED, såklart).
- Kortläsare tillika dörröppnare till LiTHe kods föreningsrum.
- RGB-pimpa fikavagnen.
- Bygg tangentbord med LiTHe kod. Vi köper in delarna. Du dyker upp, får hjälp
  med att sätta ihop allt och att programmera firmware med QMK/ZMK.
- En egen arkadmaskin. Eventuellt frågar vi M-verkstan om hjälp att bygga ett
  chassi. Utanpå sätter vi några fina knappar (utbytbar inmatningspanel?) och
  inuti sätter vi någon typ av styrenhet; en fullstor dator skulle såklart få
  plats men något med mer retro-inspirerade begränsningar som en Arduino eller
  rentav en helt egen soft core vore också spännande.

Utöver projekt har vi också en liten samling med Arduino Uno, Raspberry Pi och
tillhörande komponenter. Vi hjälper dig gärna komma igång om du vill testa att
programmera en något mindre dator än du kanske är van vid.
