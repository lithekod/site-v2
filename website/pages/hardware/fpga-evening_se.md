# FPGA-introduktionskväll den 2024-02-22

<div id="important-information">

<p>
2024-02-22: Sidan uppdaterades med schema och material.
</p>

</div>

Hårdvarugruppens första evenemang kommer vara en FPGA-introduktionskväll på
Goto10. Vi kommer först gå igenom vad en FPGA är och hur du programmerar den.
Sen kommer du få möjligheten att programmera en FPGA själv och styra en
RGB-list! Här har vi samlat den viktigaste informationen och några resurser som
kanske kommer komma till användning. Läs gärna mer på [eventsidan på
goto10.se](https://www.goto10.se/event/fpga-introduktionskvall-med-lithe-kod/).

## Material

[Presentations-pdf](/static/hardware/fpga-evening.pdf)

För att ladda upp kod till en FPGA (på Linux) behöver du troligen lägga till en
så kallad udev-regel. Detta gör du genom att

1. Skapa en fil i mappen `/etc/udev/rules.d/`.
2. Lägg till din användare i plugdev-gruppen, t.ex. `sudo usermod -aG plugdev $(whoami)`

### Go board

- Filnamn: `/etc/udev/rules.d/53-lattice-ftdi.rules`
- Innehåll: `ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", MODE="0660", GROUP="plugdev", TAG+="uaccess"`

## Schema

- 17.00: Mingel
- 17.30: Presentation
- 18.00~: Pizza time, kort paus
- 19.00~: Workshop
- 20.30: Börja packa ihop (senast)
- 21.00: Goto10 stänger.

## Pizza

**Uppdatering (2024-02-22)**: Det är numera för sent för att beställa pizza. Ta med
egen mat!

Vi kommer göra en gemensam pizza-beställning av begränsad storlek. För att
reservera en pizza-plats, maila mig som arrangerar, Gustav Sörnäs, på
[hw@lithekod.se](mailto:hw@lithekod.se) med namn och LiU-ID.

Det finns tillgång till mikrovågnsugnar om du hellre vill ta med egen mat.

## Spade

Vi kommer programmera i Spade, ett nytt hårdvarubeskrivande språk som utvecklas
på LiU. Här har vi samlat instruktioner för att komma igång med Spade.

### Linux

1. Installera Rust och byggsystemet Cargo, förslagsvis med hjälp av `rustup`
   antingen via din distributions pakethanterare eller från
   [rustup.rs](https://rustup.rs). Du kan självfallet också installera Rust och
   Cargo direkt via distributionen, men eftersom Spade kräver en ganska ny
   version av Rust är det inte säkert att det kommer fungera.
2. Installera Spades byggsystem Swim: `cargo install --git
   https://gitlab.com/spade-lang/swim` (lång kompilering).
3. Installera `oss-cad-suite`, en binärdistribution med verktyg för digital
   design: `swim install-tools` (stor nedladdning). Användbart, men behövs inte för att skriva kod.
4. `git clone https://gitlab.com/lithekod/hardware/fpga-ws2812`
5. Bygg RGB-repot: `cd fpga-ws2812; swim build` (synnerligen lång kompilering).

### macOS

Vi har inte tillgång till en macOS-dator och kan därför inte testa själva.

### Windows

Enligt uppgift går det bra att installera Swim och Spade i WSL. Börja med till
exempel
[https://learn.microsoft.com/en-us/windows/wsl/install](https://learn.microsoft.com/en-us/windows/wsl/install)
och följ sedan Linux-instruktionerna ovan.
