# FPGA introduction evening on 2024-02-22

We will have our first event, an FPGA introduction evening at Goto10! We will
first talk about what an FPGA is and how you program it. Then you will get the
opportunity to program an FPGA yourself and control an RGB strip! There is
quite a bit of information and the hardware group is very small, so for more
information you will have to check the Swedish page and translate as necessary.

Fortunately, all material will be in English. If you have any questions at all,
feel free to get in contact with Gustav Sörnäs at
[hw@lithekod.se](mailto:hw@lithekod.se).
