<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Back to LiU Game Jam's main page](/gamejam/)

# Winter Game Jam 2024

<div id=important-information>
<p>
While you're waiting, come <a href="https://discord.gg/tP2kDvgQKn">hang out in our Discord server</a>!
</p>
</div>

<img src="/static/img/gamejam/banner-wgj24.png" alt="Winter Game Jam 2023" id="gamejam-banner">

Get ready for the first jam of the year, Winter Game Jam, 2-4 February! Join us
and build kickass games together during a weekend.

<!--
### Workshop

Are you interested in game development, but it seems way too big, difficult or advanced? Do not be afraid!
Our game jam workshop is open to everyone, regardless of experience, and will teach you what you need to know to
create your own game during our game jam.

The workshop is held twice, 9th and 14th November starting at 17:30 and is expected to last approximately 2 hours.
We will go through art, game design and how to make a first game in the Godot game engine.
-->

<!--
### Information during the jam
-->

### Location

The game jam will take place on site at [Goto 10](https://www.goto10.se/linkoping/) in
Linköping, Sweden. The location has Wi-Fi but no physical network outlets.

### How to participate

In order to participate in the jam, show up at Goto 10 at (or a bit before)
18:00 February 2nd. Bring the equipment you need in order to create games:
laptop and peripherals, or physical game components.

Also, optionally:

0. Join the [itch.io jam page](https://itch.io/jam/liu-game-jam-winter-game-jam-2024) where we would like for you to upload
   the resulting game too, so it is more accesible and can be added to our game
   archive:
   [Itch.io Archive](https://itch.io/c/64050/liu-game-jam)
0. Join our Discord to get announcements during the jam:
   [Discord invite](https://discord.gg/tP2kDvgQKn)
0. Join the [Facebook event](https://www.facebook.com/events/1077978989857126)<br/>*This helps us
   approximate the number of attending jammers for better planning.*

### Schedule

**NOTE**: No shared buffet or sushi order this jam.

**Friday 2 February**

- 18:00: Introduction
    - Theme reveal
    - Brainstorming
    - Group forming
- 18:10: Joint pizza order deadline

**Saturday 3 February**

- 09:00: Spektrum opens
    - Breakfast (free)
- Around 18:15: Half time review

**Sunday 4 February**

- 09:00: Spektrum opens
    - Breakfast (free)
- 18:00: Game creation deadline
    - Resulting games should be uploaded to globalgamejam.org and itch.io
    - Jammers should have cleaned their tables
- 18:15: Final review
- 19:00: Playtesting and showcase

### Theme

The themes are generated at the start of the jam by taking random pairs of words
submitted by participants. They will be added at the top of this page after the start of the jam.

Theme word rules:

- Should only be one English word.
- Should not be a game genre (e.g. FPS, RTS).
- Keep it safe for work.

Our theme process ensures unique and challenging themes each jam. The themes are
there for inspiration during brainstorming but you can pick something else as
well.
