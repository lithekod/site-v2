<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Back to LiU Game Jam's main page](/gamejam/)

# Spring Game Jam 2024

<div id=important-information>
<p>
While you're waiting, come <a href="https://discord.gg/tP2kDvgQKn">hang out in our Discord server</a>!
</p>
</div>

<img src="/static/img/gamejam/banner-sgj24.png" alt="Spring Game Jam 2023" id="gamejam-banner">

Get ready for our next jam, Spring Game Jam, 12-14 April! Join us
and build kickass games together during a weekend.

<!--
### Workshop

Are you interested in game development, but it seems way too big, difficult or advanced? Do not be afraid!
Our game jam workshop is open to everyone, regardless of experience, and will teach you what you need to know to
create your own game during our game jam.

The workshop is held twice, 9th and 14th November starting at 17:30 and is expected to last approximately 2 hours.
We will go through art, game design and how to make a first game in the Godot game engine.
-->

<!--
### Information during the jam
-->

### Location

The game jam will take place on site at [Spektrum](https://www.ebbepark.se/) in
Linköping, Sweden. The location has Wi-Fi but no physical network outlets.

### How to participate

In order to participate in the jam, show up at Spektrum at (or a bit before)
18:00 April 12th. Bring the equipment you need in order to create games:
laptop and peripherals, or physical game components.

Also, optionally:

0. Join the [itch.io jam page](https://itch.io/jam/liu-game-jam-spring-game-jam-2024) where we would like for you to upload
   the resulting game too, so it is more accesible and can be added to our game
   archive:
   [Itch.io Archive](https://itch.io/c/64050/liu-game-jam)
0. Join our Discord to get announcements during the jam:
   [Discord invite](https://discord.gg/tP2kDvgQKn)
0. Join the [Facebook event](https://www.facebook.com/events/655522303333797/?acontext=%7B%22event_action_history%22%3A[%7B%22extra_data%22%3A%22%22%2C%22mechanism%22%3A%22surface%22%2C%22surface%22%3A%22create_dialog%22%7D%2C%7B%22extra_data%22%3A%22%22%2C%22mechanism%22%3A%22surface%22%2C%22surface%22%3A%22permalink%22%7D%2C%7B%22extra_data%22%3A%22%22%2C%22mechanism%22%3A%22surface%22%2C%22surface%22%3A%22edit_dialog%22%7D]%2C%22ref_notif_type%22%3Anull%7D)<br/>*This helps us
   approximate the number of attending jammers for better planning.*

### Schedule

**NOTE**: No shared buffet order this jam.

**Friday 12 April**

- 18:00: Introduction
    - Theme reveal
    - Brainstorming
    - Group forming
- 18:15: Joint pizza order deadline

**Saturday 13 April**

- 09:00: Spektrum opens
    - Breakfast (free)
- Around 12:00: Joint sushi order deadline
- Around 18:15: Half time review

**Sunday 14 April**

- 09:00: Spektrum opens
    - Breakfast (free)
- 16:30: Game creation deadline
    - Resulting games should be uploaded to itch.io
    - Jammers should have cleaned their tables
- 16:45: Final review
- 17:30: Playtesting and showcase

### Theme

The themes are generated at the start of the jam by taking random pairs of words
submitted by participants. They will be added at the top of this page after the start of the jam.

Theme word rules:

- Should only be one English word.
- Should not be a game genre (e.g. FPS, RTS).
- Keep it safe for work.

Our theme process ensures unique and challenging themes each jam. The themes are
there for inspiration during brainstorming but you can pick something else as
well.
