<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Tillbaka till huvudsidan för LiU Game Jam](/gamejam/)

# Spring Game Jam 2024

<div id=important-information>
<p>
Medan du väntar, <a href="https://discord.gg/tP2kDvgQKn">kom å häng i vår Discord-server</a>!
</p>
</div>

<img src="/static/img/gamejam/banner-sgj24.png" alt="Spring Game Jam 2024" id="gamejam-banner">

Gör dig redo för nästa game jam, Sprig Game Jam, den 12-14 april! Häng
med och bygg grymma spel tillsammans under en helg.

<!--
### Workshop

Är du intresserad av spelutveckling, men det verkar alldeles för stort, svårt eller avancerat? Var inte rädd!
Vår game jam-workshop är öppet för alla, oavsett tidigare erfarenhet, och lär dig det du behöver för att kunna
skapa ditt egna spel under vårt game jam.

Workshoppen hålls två gånger, 9:e och 14:e november med starttid 17:30 och förväntas hålla på i ungefär 2 timmar.
Vi kommer gå igenom art, speldesign och hur man gör ett första spel i spelmotorn Godot. 
-->


<!--
### Information under jammet
-->

### Lokal

Game-jammet kommer vara på [Spektrum](https://www.ebbepark.se/) i Linköping. Lokalen har Wi-Fi men inga fysiska nätverksuttag.

### Delta

För att delta på evenemanget: kom till Spektrum klockan 18:00 (eller lite tidigare) fredag den 12 april. Ta med det du behöver för att bygga spel; laptop eller fysiska spelkomponenter.

Valfritt:

0. Gå med på [Itch.io-sidan](https://itch.io/jam/liu-game-jam-spring-game-jam-2024) där du gärna får ladda upp resultatet så att det blir mer tillgängligt och kan läggas till i vårt spelakriv:
[Itch.io-arkiv](https://itch.io/c/64050/liu-game-jam).
0. Gå med i [vår Discord-server](https://discord.gg/tP2kDvgQKn) för att få ny information under jammet.
0. Gå med i
[Facebook-evenemanget](https://www.facebook.com/events/655522303333797/?acontext=%7B%22event_action_history%22%3A[%7B%22extra_data%22%3A%22%22%2C%22mechanism%22%3A%22surface%22%2C%22surface%22%3A%22create_dialog%22%7D%2C%7B%22extra_data%22%3A%22%22%2C%22mechanism%22%3A%22surface%22%2C%22surface%22%3A%22permalink%22%7D%2C%7B%22extra_data%22%3A%22%22%2C%22mechanism%22%3A%22surface%22%2C%22surface%22%3A%22edit_dialog%22%7D]%2C%22ref_notif_type%22%3Anull%7D).
<br/>*Det hjälper oss att uppskatta hur många som kommer delta, för att kunna planera jammet bättre.*

### Schema

**OBS**: Ingen delad buffet det här jammet.

**Fredag 12 april**

- 18:00 Introduktion
    - Temareveal
    - Brainstorming
    - Gruppskapande
- 18:15 Gemensam pizza-beställning (deadline)

**Lördag 13 april**

- 09:00 Spektrum öppnar
    - Frukost (gratis)
- Runt 12:00: Deadline för gemensam sushi-beställning
- Runt 18:15: Halvtidsredovisning med sushi

**Söndag 14 april**

- 09:00 Spektrum öppnar
    - Frukost (gratis)
- 16:30 Game Jam-deadline
    - Spelen ska vara uppladdade till itch.io
    - Deltagare ska ha städat sina bord och sin omgivning
- 16:45: Slutredovisning
- 17:30: Speltestning och uppvisning

### Tema

Teman för eventet skapas under introduktionen genom att para ihop ord som lämnats in av deltagarna.

Regler för tema-ord:

- Ska bara vara ett enda engelskt ord.
- Ska inte vara en spelgenre (ex. FPS, RTS).
- Undvik osedligt innehåll. (Ska vara safe for work.)

Vår temaprocess skapar unika och utmanande teman varje jam. De finns som inspiration under brainstormsessionen, men du kan bygga efter egna idéer också.
