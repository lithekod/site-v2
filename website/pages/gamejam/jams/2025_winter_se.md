<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Tillbaka till huvudsidan för LiU Game Jam](/gamejam/)

# Winter Game Jam 2025: 31 januari - 2 februari

<div id=important-information>
<p>
Medan du väntar, <a href="https://discord.gg/tP2kDvgQKn">kom å häng i vår Discord-server</a>!
</p>
</div>

<img src="/static/img/gamejam/banner-wgj25.png" alt="Winter Game Jam 2025" id="gamejam-banner">

Gör dig redo för nästa game jam, Winter Game Jam, 31 januari - 2 februari! Häng
med och bygg grymma spel tillsammans under en helg.

<!--
### Workshop

Är du intresserad av spelutveckling, men det verkar alldeles för stort, svårt eller avancerat? Var inte rädd!
Vår game jam-workshop är öppet för alla, oavsett tidigare erfarenhet, och lär dig det du behöver för att kunna
skapa ditt egna spel under vårt game jam.

Workshoppen hålls två gånger, 20:e och 26:e november med starttid 18:00 och förväntas hålla på i ungefär 2 timmar.
Vi kommer gå igenom art, speldesign, lite musik och hur man gör ett första spel i spelmotorn Godot.


### Information under jammet
-->

### Lokal

Game-jammet kommer vara på [Spektrum](https://www.ebbepark.se/) i Linköping.
Lokalen har Wi-Fi men inga fysiska nätverksuttag.

### Delta

För att delta på evenemanget: kom till Spektrum klockan 18:00 (eller lite
tidigare) fredag den 31 januari. Ta med det du behöver för att bygga spel;
laptop eller fysiska spelkomponenter.

Valfritt:

0. Gå med på
   [Itch.io-sidan](https://itch.io/jam/liu-game-jam-winter-game-jam-2025) där du
   gärna får ladda upp resultatet så att det blir mer tillgängligt och kan
   läggas till i vårt spelakriv:
   [Itch.io-arkiv](https://itch.io/c/64050/liu-game-jam).
1. Gå med i [vår Discord-server](https://discord.gg/tP2kDvgQKn) för att få ny
   information under jammet.
2. Gå med i [Facebook-evenemanget](https://fb.me/e/5ShEmA0UD).
   <br/>_Det hjälper oss att uppskatta hur många som kommer delta, för att kunna
   planera jammet bättre._

### Schema

**Fredag 31 januari**

- 18:00 Introduktion
  - Temareveal
  - Brainstorming
  - Gruppskapande
- 18:30 Gemensam pizza-beställning (deadline)

**Lördag 1 februari**

- 09:00 Spektrum öppnar
  - Frukost (gratis)
- Runt 12:00: Deadline för gemensam BanChan och sushi-beställning
- Runt 18:15: Halvtidsredovisning med BanChan

**Söndag 2 februari**

- 09:00 Spektrum öppnar
  - Frukost (gratis)
- 12:00 Sushi leverans
- 15:30 Game Jam-deadline
  - Spelen ska vara uppladdade till itch.io
  - Deltagare ska ha städat sina bord och sin omgivning
- 15:45: Slutredovisning
- 16:30: Speltestning och uppvisning

### Tema

Teman för eventet skapas under introduktionen genom att para ihop ord som
lämnats in av deltagarna.

Regler för tema-ord:

- Ska bara vara ett enda engelskt ord.
- Ska inte vara en spelgenre (ex. FPS, RTS).
- Undvik osedligt innehåll. (Ska vara safe for work.)

Vår temaprocess skapar unika och utmanande teman varje jam. De finns som
inspiration under brainstormsessionen, men du kan bygga efter egna idéer också.
