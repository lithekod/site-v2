<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

Välkommen till LiU Game Jam! Vi är en undergrupp till LiTHe kod som anordnar tre
game jams per år.

På den här sidan kan du läsa om vårt [nästkommande game jam](jams/2025-winter/),
kolla igenom [våra gamla jams](jams/) och läsa på om
[bra verktyg för game jams](tools/).

## Winter Game Jam 2024: 31 januari - 2 februari

<img src="/static/img/gamejam/banner-wgj25.png" alt="Winter Game Jam 2025" id="gamejam-banner">

<!--
<a href="jams/2024-spring/"><img src="/static/img/gamejam/banner-sgj24.png" alt="Spring Game Jam 2024" id="gamejam-banner"></a>
-->

Vårt nästa game jam är [Winter Game Jam 2025](jams/2025-winter/). Läs mer på
jam-sidan och värm upp i [vår Discord-server](https://discord.gg/tP2kDvgQKn)! Vi
har också anordnat två workshops, mer information finns nedan.

## Game jam workshop

Första game-jam? Osäker på hur du kommer igång? Inga problem! Vi har arrangerat
workshops för just detta och materialet är uppladdat på sidan. Det är bara in
och kika på vår [gamla workshop-sida](workshop/2023)

---

## Vad är ett game jam?

Ett game jam är ett event där programmerare, konstnärer och gamers går ihop för
att skapa spel; både datorspel och brädspel. Alla våra game jams har sina egna
slumpmässigt valda teman för inspiration och en 48-timmarsgräns att skapa sitt
spel på!

## Andra kanaler

LiU Game Jam kan följas i dessa kanaler.

- [Discord](https://discord.gg/tP2kDvgQKn)
- [Facebook](https://www.facebook.com/liugamejam/)
- [Nyhetsbrev](http://us12.campaign-archive2.com/home/?u=092a6fffba8f6063437a51495&id=c3863c4bf5)
- [Spelarkiv på Itch.io](https://itch.io/c/64050/liu-game-jam)

## FAQ

### Vem är välkommen?

Alla är välkomna, både studenter och icke-studenter. Ingen förkunskap krävs och
evenemangen är gratis.

### Kan jag delta i eventet på distans?

Vi vill att detta ska vara ett fysiskt event. Du kan arbeta remote fast
introduktion, redovisning och speltestning kommer vara på plats. Se till att du
eller andra gruppmedlemmar kan vara där.

### Hur väljs grupper? Behöver jag ett team?

Grupper kommer sättas ihop slumpmässigt för brainstorming, men du kan arbeta med
vem du vill. Startgrupperna ger dig möjlighet att hitta människor med liknande
idéer som dig. Vi ser till att alla hittar någon att arbeta med.

### Behöver jag följa temat?

Temat finns för inspiration under brainstorming-sessionen. Om du har en egen idé
eller arbetar på ett existerande projekt kan du arbeta på det också.

### Behöver jag kunna programmera?

Nej, inte alls! Speldesign täcker alla möjliga områden så som konst, musik,
design, storywriting, nivå-skapning och problemlösning. Alla som vill skapa är
välkomna.

### Vad för verktyg kan jag använda för att bygga mitt spel?

Vad som helst, en massa deltagare använder spelmotorer för att snabbt komma
igång. En del andra bygger egna programmeringsspråk, spelmotorer eller använder
Google Spreadsheets. En bra lista över resurser kan hittas i
[vår lista över verktyg](tools/).

### Kan jag använda asset packs eller återanvända kod?

Ja, använd allt du har för att bygga ett grymt spel!
