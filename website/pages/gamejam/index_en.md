<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

Welcome to LiU Game Jam! We are a subgroup in LiTHe kod who organizes three game
jams every year.

Here you can read about our [next game jam](jams/2025-winter/), check out
[our old jams](jams/) and read up on [great tools for game jams](tools/).

## Winter Game Jam 2025: 31 January - 2 February

<img src="/static/img/gamejam/banner-wgj25.png" alt="Winter Game Jam 2025" id="gamejam-banner">

<!--
<a href="jams/2024-spring/"><img src="/static/img/gamejam/banner-sgj24.png" alt="Spring Game Jam 2024" id="gamejam-banner"></a>
-->

Our next game jam will be [Winter Game Jam 2025](jams/2025-winter/). Read more
on the jam-page and prepare in
[our Discord server](https://discord.gg/tP2kDvgQKn)! We have also organized two
workshops which you can read more about below.

## Game jam workshop

First game jam? Unsure on where to start? No worries! We have organized
workshops specifically for new jammers in the past and all material is available
online under our [old workshop page](workshop/2023)

---

## What is a game jam?

A game jam is an event where programmers, artists and gamers come together to
create games; both computer games and board games. At our game jam there is a 48
hours time limit and a randomized theme for inspiration.

## Other channels

Follow LiU Game Jam on the following channels.

- [Discord server](https://discord.gg/tP2kDvgQKn)
- [Facebook page](https://www.facebook.com/liugamejam/)
- [Newsletter](http://us12.campaign-archive2.com/home/?u=092a6fffba8f6063437a51495&id=c3863c4bf5)
- [Itch.io game archive](https://itch.io/c/64050/liu-game-jam)

## FAQ

### Am I welcome?

All are welcome, student and non-student alike. No previous experience is needed
and the event is free of charge.

### Can I join the jam remotely?

Not completely as we want to keep this game jam on site. You are free to work
remotely but introduction, review and play testing events will be on site. Make
sure you can visit for them or have a team member there to represent you.

### How are groups selected? Do I need a team?

Groups will be randomized for brainstorming purposes, but you are free to work
with whomever you like. The starting groups will allow you to find people
sharing similar ideas. We make sure everyone finds someone to work with.

### Do I need to have a team to participate?

You can work alone or in teams. It is more fun to jam in teams so we highly
encourage you to find one.

### Do I need to follow the theme?

The theme is there for inspiration during brainstorming. If you have your own
idea or an already existing project feel free to work on it.

### Do I need to be able to program?

Not at all! Game design covers all kind of fields such as art, music, design,
storywriting, level creation and problemsolving. Anyone who is eager to create
is welcome.

### What can I make my game in?

Anything, a lot of jammers favor using game engines for the ease of use but
participants have used everything from custom made programming languages to
Google spreadsheets. A good list of resources can be found in
[our list of tools](tools/).

### Can I use assets packs or reuse code?

Yes, feel free to use anything you have to make a kickass game.
