<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam Workshop</h1>
</div>

Vi i LiU Game Jam anordnar workshoppar för att ge nya game jam-deltagare en
introduktion i game jam-spelskapandet! Under en kväll kommer vi gå igenom
typiska bra saker att ha koll på under ett game jam.

## Vad händer på workshoppen?

Vi kommer gå över tre centrala delar av spelskapandet (plus _lite_ spelmusik):

- Brainstorming,
- Arts och Graphics,
- Spelprogrammering i spelmotorn Godot

## Var/när är workshoppen?

Workshoppsen kommer hållas 20:e och 26:e november i salen Ada Lovelace (B-huset, ingång 27).
Starttiden är 18:00 och den förväntas hålla på i ca 2 timmar.
Vi kommer gå igenom samma material på båda så gå på den som passar dig bäst.

## Om jag inte kan närvara på någon av dem?

Inga problem, att närvara vid en workshop är inget obligatoriskt,
det är till för lite extra hjälp. Vi har laddat upp [materialet från förra
årets workshops](2023/).
Där hittar du presentationer, spelen samt deras källkod.
