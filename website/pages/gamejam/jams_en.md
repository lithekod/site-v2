<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Back to LiU Game Jam's main page](/gamejam/)

# History

LiU Game Jam has been been around since 2012. It started as its own group but joined forces with LiTHe kod in 2015.
In connection to the jams we have also arranged workshops in a number of different game engines and tools.

Check out games from our jams in the [archive collection](https://itch.io/c/64050/liu-game-jam).
You can also have a look at our [old newsletters](http://us12.campaign-archive2.com/home/?u=092a6fffba8f6063437a51495&id=c3863c4bf5).

The following are all jams organized by us, listed together with their theme(s) and relevant links where applicable:

### 2024

- Spring Game Jam 2024 - _"Arch goats", "Reverse Tophats", "Hide Labyrinth"_ - [Itch.io](https://itch.io/jam/liu-game-jam-spring-game-jam-2024), [Intro](https://docs.google.com/presentation/d/16gN6yQQS7ut6kXHTrdpVs_WXVISuKuc-dntHEjrBFV0/edit#slide=id.g2ca12c4e321_2_8), [Half-time review](https://docs.google.com/presentation/d/1W55w1LVAmIDMQBMCXZ6H8AF5T6AQIMk6s9DF7JI_ca8/edit#slide=id.g17c946ca458_0_56), [Presentation](https://docs.google.com/presentation/d/1d-jzBC9QGt17SjoTk_AVJUTcxuukJ_uvU7k4lxisOmY/edit#slide=id.g17c946ca458_0_56)
- Winter Game Jam 2024 - _"Necrotic shovel", "History gurka", "Baseball skor"_ - [Itch.io](https://itch.io/jam/liu-game-jam-winter-game-jam-2024), [Intro](https://docs.google.com/presentation/d/1dt6mXPjkW_S_O0m90vgr6mUDT2KwQNjhq6HVA--XzXM/edit#slide=id.g17b3769eb49_0_4), [Half-time review](https://docs.google.com/presentation/d/1MKDB-2zSHxkmDGkY1l42wUkqXEOhxxbBhWJgTllEg6Y/edit#slide=id.g2b629ded0ca_43_0), [Presentation](https://docs.google.com/presentation/d/1Ufv-LTtdWop-IuANeDjLPTjNK2xEu6B9HKuq665Beh0/edit#slide=id.g2b64b24ea9c_48_0)

### 2023

- Fall Game Jam 2023 - _"Ageing Trampoline", "Bread Crops", "Turtle Switch"_ - [Itch.io](https://itch.io/jam/liu-fall-game-jam-2023), [Intro](https://docs.google.com/presentation/d/1Kul9mFjcVtQy5frfq4jRRvT8iRih2hgbi1BnZQthBek/edit#slide=id.g17b3769eb49_0_4), [Half-time review](https://docs.google.com/presentation/d/1pSz5Nrnl5dPRaUDpdsGxmCMmJLGmCBySHn5c0_gU34k/edit#slide=id.g17c946ca458_0_67), [Presentation](https://docs.google.com/presentation/d/1Q2JAv0gaBqwXcNqZpcYuhsg8rdaJUzG8AN2i0FED0IU/edit#slide=id.g17c946ca458_0_56)
- Spring Game Jam 2023 - _"Val (fast på engelska) miximum", "Fruit shark", "FOMO - profitable"_ - [Itch.io](https://itch.io/jam/liu-spring-game-jam-2023), [Intro](https://docs.google.com/presentation/d/1FyD0zhU139LQBHl6ymr-HcEqOxinEF9ViDGFM_HAtqA/edit?usp=sharing), [Half-time review](https://docs.google.com/presentation/d/11pUub6vZe744Sr2aQ4hMn1AZpjot_E6ThhyElb3sx3E/edit?usp=sharing), [Presentation](https://docs.google.com/presentation/d/1GaNcvpmFdvYKU-Uz-aDNOYLCTcDSkqCmxOyHawLhYoM/edit?usp=sharing)
- Global Game Jam Linköping 2023 - _"Roots"_ - [Itch.io](https://itch.io/jam/global-game-jam-linkoping-2023), [Intro](https://docs.google.com/presentation/d/1EA3n-ThSHvlgNE4G2nQvKbYcvy4TSG7nsjvsMl5ZRXU/edit?usp=sharing), [Half-time review](https://docs.google.com/presentation/d/1UjYZYZBvChAFsoXxSmuXvchZZ1m_ujrgJVpx3eva3O8/edit?usp=sharing), [Presentation](https://docs.google.com/presentation/d/1A9eh3X_v_os2szcBEg5TiPxiJD3irqZCvdezA5mL1Ns/edit?usp=sharing)

### 2022

-   Fall Game Jam 2022 - _"Roman Cages", "Inside-out Gardening", "Radiating Mimics"_ - [Itch.io](https://itch.io/jam/liu-fall-game-jam-2022), [Intro](https://docs.google.com/presentation/d/1jzRUNTo4vnVF5JossQa-jlZC5vNmzO0d2TLGNs_hJfQ/edit?usp=sharing), [Half-time review](https://docs.google.com/presentation/d/1Z3sl2VyVlKPWGVz1XAX8Gy3D0bwck41sddYiAWMGWnE/edit?usp=sharing), [Presentation](https://docs.google.com/presentation/d/1E-L2Khk5zTP4tLuA2wuJml-4XA4-QzTFbJSypIO7d88/edit?usp=sharing)
-   Spring Game Jam 2022 - _"Banana Energy", "Coral-reef Monkey", "Cthulhu Capybara"_ - [Itch.io](https://itch.io/jam/spring-game-jam-2022/entries), [Intro](https://docs.google.com/presentation/d/1N16S1CwCrhD45l4MuABPXQyD9EIOmpdiU4mXwLfuH1g/edit?usp=sharing), [Half-time review](https://docs.google.com/presentation/d/1ZpSNFklXwhvTgO6uHj-yqyx6l-fvToWHU7DS7ZRBlMA/edit?usp=sharing), [Presentation](https://docs.google.com/presentation/d/1gKwcsF_iCu5AqUBUjfJAWL8zFdzq7CFOMZdumaj0tlQ/edit?usp=sharing)
-   Global Game Jam Linköping 2022 - _"Duality"_ - [Itch.io](https://itch.io/jam/global-game-jam-linkping-2022), [Global Game Jam](https://globalgamejam.org/2022/jam-sites/liu-game-jam), [Intro](https://www.youtube.com/watch?v=TfqLkhcl_4I), [Presentation](https://www.youtube.com/watch?v=pB0z86u8D-s)

### 2021

-   Fall Game Jam 2021 - _"Dragon Egg", "Cute Sandwich", "Constructive Bombs"_ - [Itch.io](https://itch.io/jam/liu-fall-game-jam-2021), [Intro](https://docs.google.com/presentation/d/e/2PACX-1vSMsd02sIr97sLytGm9vdSH1lkK1jzBug3eQ1KSDuhNWkpiWBe8grH3vuYRoHneOP34hnud-uXhwrDF/pub?start=false&loop=false&delayms=3000), [Half-time review](https://docs.google.com/presentation/d/e/2PACX-1vTkudkXWGC4XbxbzKGACQH6mS6Lmk-nMogFAgHQcE81RuxXydJBa-siG0KGW4Gnhtc104OC7s0vmkYI/pub?start=false&loop=false&delayms=5000), [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vQPeahI0YlsJIW31L3H5CIN9pAom19ZaW_ivshDXmIuvZjkq06WeY-IvHwtHQx0HTU7Afk9dhBWQ7Vj/pub?start=false&loop=false&delayms=3000)
-   Summer Game Jam 2021 - _"Animal Hockey", "Anti-Wholesome", "Dog Pistachio"_ - [Itch.io](https://itch.io/jam/liu-summer-jam-2021), [Intro](https://www.youtube.com/watch?v=39uLKI7OyIY), [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vQ_8qvsSzfm3go0VfucBzxXQJfAXBsnXXj2PhTu8zVyCYoqMgBOolr9isxPz64Mgh6WcrhcsJsqwvvw/pub?start=false&loop=false&delayms=3000)
-   Spring Game Jam 2021 - _"Friendly Frisbee", "Wine Language", "Deadline Capitalism"_ - [Itch.io](https://itch.io/jam/spring-game-jam-2021), [Intro](https://www.youtube.com/watch?v=SDf2q96JG9k), [Presentation](https://www.youtube.com/watch?v=0_zV3FEJG04)
-   Global Game Jam Linköping 2021 - _"Lost and Found"_ - [Itch.io](https://itch.io/jam/global-game-jam-linkoping-2021), [Global Game Jam](https://globalgamejam.org/2021/jam-sites/liu-game-jam), [Intro](https://www.youtube.com/watch?v=yKhhIuSa49M), [Presentation](https://www.youtube.com/watch?v=lEk12BYQYgo)

### 2020

-   Fall Game Jam 2020 - _"Distance Plain", "Giant 2020"_ - [Itch.io](https://itch.io/jam/liu-fall-game-jam-2020),
    [Intro](https://www.youtube.com/watch?v=wJ1pcaT8opk), [Presentation](https://www.youtube.com/watch?v=MpBZgX6GBuY)
-   Spring Game Jam 2020 - _"Orange Expansions", "Ghoul Reassembly"_ - [Itch.io](https://itch.io/jam/liu-spring-game-jam-2020),
    [Intro](https://www.youtube.com/watch?v=BXhOfB7rdMs), [Presentation](https://www.youtube.com/watch?v=1-98sMfigc4)
-   Global Game Jam Linköping 2020 - _"Repair"_ - [Global Game Jam](https://globalgamejam.org/2020/jam-sites/liu-game-jam-ggj-2020), [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vTXBO6GEullpwOgIFVzQ0z8-3Al8WO-j775CVlnbyFj3hBFoRljemy4-bXKgB-PqJD4C6y-kTZKjbV3/pub?start=false&loop=false&delayms=3000)

### 2019

-   Fall Game Jam 2019 - _"Wild West Planes, but with eyes", "Compost War: Two worlds, one truck", "Musical tentacles"_ - [Presentation](https://www.facebook.com/349988561805129/videos/799393110511100)
-   Spring Game Jam 2019 - _"Spinning Change", "GODZILLA In Real Life", "Very large Pizza cutter"_ - [Presentation](https://www.facebook.com/349988561805129/videos/297587374524542)
-   Global Game Jam Linköping 2019 - _"What home means to you"_ - [Global Game Jam](https://globalgamejam.org/2019/jam-sites/liu-game-jam/games), [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vSYxPDiq7GMtCeC_-KOQeQtQECAs2ksc9fTglwDGAaRXVgtL2HMh0GTpfj12873_mC3K98p_rrwW-Sh/pub?start=false&loop=false&delayms=3000)

### 2018

-   Fall Game Jam 2018 - _"Metal Time travel", "Navy Nightmare", "Big sound House"_ - [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vS0Cu3etDiGNylFSJU0xKftrokjhYdp0Q1Iu2-7dL-pQ_b2TupyQfF5DyaaPcuzNnuZdISjGKraDBWI/pub?start=false&loop=false&delayms=3000)
-   Spring Game Jam 2018 - _"Extreme-dancing King", "Coyote Solitude"_ - [Presentation](https://www.facebook.com/349988561805129/videos/1279817882155521)
-   Global Game Jam 2018 Linköping - _"Transmission"_ - [Global Game Jam](https://globalgamejam.org/2018/jam-sites/liu-game-jam)

### 2017

-   Fall Game Jam 2017 - _"Royal Styling", "Flat abuse", "Speedy Cactus"_ - [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vRWTXIbYhm5tWQC9cf-3m9GOow5nFtsNVNHsvNg3HatSJctKyNAWbj0bjKktF1BeXYdYFAeQ9TQp08k/pub?start=false&loop=false&delayms=3000)
-   Spring Game Jam 2017 - _"Highschool Race", "Icecream monsters", "Pixel Erasers"_ - [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vRn0hLicCmqP_Sig2LHsfagh3TRj63wjCKMlUCROW0wAWMbnuk7xXgZuY3nD6Qjy0JBhrgeG1VZj4iT/pub?start=false&loop=false&delayms=3000)
-   Global Game Jam 2017 Linköping - _"Waves"_ - [Global Game Jam](https://globalgamejam.org/2017/jam-sites/liu-game-jam)

### 2016

-   Fall Game Jam 2016 - _"Duck Crabs", "Chocolate Guitars", "Angry monkeys (but with more legs)"_
-   Spring Game Jam 2016 - _"Zombie Feelings", "Pink Song-contest", "Mountain Chicken"_
-   Global Game Jam 2016 - _"Ritual"_ - [Global Game Jam](https://globalgamejam.org/2016/jam-sites/liu-game-jam)

### 2015

-   Fall Game Jam 2015 - _"Roundabout Battle", "Raging Change"_
-   Spring Game Jam 2015 - _"Disruptive rabbits"_
-   Global Game Jam 2015 - _"What do we do now"_ - [Global Game Jam](https://globalgamejam.org/2015/jam-sites/liu-game-jam)

### 2014

-   Fall (of the) Game Jam 2014 - _"Rocket Castle"_
-   Global Game Jam 2014 - _"We don't see things as they are, we see them as we are"_ - [Global Game Jam](https://globalgamejam.org/2014/jam-sites/liu-game-jam)

### 2013

-   Fall Game Jam 2013 - _"Space Stegosaurus"_
-   Spring Game Jam 2013

### 2012

-   Global Game Jam 2012 - _"Ouroboros"_ - [Global Game Jam](http://archive.globalgamejam.org/og/games/18189/list.html)
