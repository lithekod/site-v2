<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam Workshop</h1>
</div>

We at LiU Game Jam will organize workshops to give new game jam participants an
introduction to game jam game creation! During one evening we will go through
typical good things to keep track of during your first game jam.

## Content

We will go over three central parts of game making (plus _some_ game music):

- Brainstorming,
- Arts and Graphics,
- Game programming in the game engine Godot

## Where/when is the workshop?

The workshops will be held November 20th and 26th in Ada Lovelace (B-house,
door number 27). The start time is 18:00 and we expect to go for 2 hours. We
will do the same material both times so choose the time that fits your schedule
best!

## What if I can't attend any of them?

Don't worry, being present at one of the workshops is not mandatory 
to attend a game jam, it is just a help. We have uploaded the
[material from last years workshops](2023/).
You will find the presentation and the two games we created together
with their source code.
