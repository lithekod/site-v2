# LiTHe kods undergrupper

LiTHe kod är numera uppdelad i undergrupper! Här kan du läsa mer om vilka
undergrupper som finns och vad dom har för sig.

Gruppledare väljs in för ett år i taget på
[vårmötet](/organization/meetings/) tillsammans med resten av styrelsen.

- [**LiU Game Jam**](/gamejam/) har anordnat game jams i Linköping sedan
  2013\. Under ett game jam får du göra ett spel från grunden under en helg,
  antingen själv eller med andra. LiU Game Jam anordnar vanligtvis tre game jams
  om året: Fall Game Jam på hösten, Global Game Jam under vintern och Spring
  Game Jam på våren.
- [**Tävlingsprogrammeringsgruppen**](/competitions/) anordnar olika typer av
  evenemang som har med tävlingsprogramamering och problemlösning att göra. På
  hösten anordnar vi [NCPC]() (tidigare programmerings-SM) med möjligheten att
  skicka vidare lag till [NWERC](). För dig som föredrar att lösa problem i egen
  takt finns [Advent of Code](/competitions/aoc/) (en adventskalender med
  programmeringsproblem varje december) och [IMPA](/competitions/impa/) (en
  programmeringstävling för universitetsstudenter som pågår under hela läsåret).
- [**Hårdvarugruppen**](/hardware/) är för dig som är intresserad av
  programmerbar hårdvara och annan hårdvarunära programmering. Vi är ganska nya
  och har därför inte så många planerade evenemang, så om du är tidigare
  intresserad eller bara vill lära dig något nytt är du varmt välkommen att höra
  av dig!
- Meetupgruppen ser till att fikavagnen har tillräckligt med kakor, kaffe och te
  för våra meetups varje tisdag.

Dessutom har vi några interna grupper som inte anordnar egen verksamhet, men
som ändå gör föreningen till vad den är.

- WWW-gruppen tar hand om LiTHe kods hemsida (som du läser just nu!) så att
  styrelsen och undergrupperna kan informera om sin verksamhet. WWW-gruppen
  underhåller också föreningens server hos Lysator och medlemsdatabasen.
- Lokalgruppen har nyckel till och ansvar över föreningens alldeles egna
  föreningsrum i Kårallen! <!--Lokalen är tillgänglig för våra undergrupper och
  medlemmar som vill ordna ett projekt. Mer information om detta får du på
  [sidan för projekt](/organization/projects/) eller genom att ta kontakt med
  vår lokalansvarig.-->
