# Kontakt

<!--
<div id="important-information">

<p>

Sök styrelsen! <a href="https://forms.gle/L5j9SQdbqMr1cDms5">Ansökan sker via
formulär.</a> Läs mer om vad de olika posterna gör här nedan.

</p>

</div>
-->

Har du någon fråga? Tveka inte att kontakta oss!
Vi nås på [Discord](https://discord.gg/UG5YYsN).

Vi har meetups **varje tisdag** i Café Java kl 17.15. Kom förbi så bjuder vi på fika!

# Styrelsen

<div id="card-container">

<div class="profile-card">
    <img src="/static/img/board-24-25/morgan.jpg" alt="Morgan">
    <h2>Morgan Nordberg</h2>
    <p class="profile-card-sub">Ordförande</p>
    <p class="profile-card-sub"><a href="mailto:ordf@lithekod.se">ordf@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/astrid.jpg" alt="Astrid">
    <h2>Astrid Lauenstein</h2>
    <p class="profile-card-sub">Vice Ordförande</p>
    <p class="profile-card-sub"><a href="mailto:vice.ordf@lithekod.se">vice.ordf@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/johannes.jpg" alt="Johannes">
    <h2>Johannes Kung</h2>
    <p class="profile-card-sub">Kassör</p>
    <p class="profile-card-sub"><a href="mailto:kassor@lithekod.se">kassor@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/kacper.jpg" alt="Kacper">
    <h2>Kacper Uminski</h2>
    <p class="profile-card-sub">Verksamhetsansvarig</p>
    <p class="profile-card-sub"><a href="mailto:verks@lithekod.se">verks@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/martin.jpg" alt="Martin">
    <h2>Martin Högstedt</h2>
    <p class="profile-card-sub">Game Jam-ansvarig</p>
    <p class="profile-card-sub"><a href="mailto:gamejam@lithekod.se">gamejam@lithekod.se</a></p>
</div>

</div>

## Övriga

<div id="card-container">

<div class="profile-card">
    <img src="/static/img/board-22-23/gustav.png" alt="Gustav">
    <h2>Gustav Sörnäs</h2>
    <p class="profile-card-sub">Revisor</p>
    <p class="profile-card-sub"><a href="mailto:revisor@lithekod.se">revisor@lithekod.se</a></p>
</div>

</div>

Gustavs profilbild är ritad av Annie Wång! Tack :D
