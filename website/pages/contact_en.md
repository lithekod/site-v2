# Contact us

<!--
<div id="important-information">

<p>

Apply to the board! <a href="https://forms.gle/L5j9SQdbqMr1cDms5">The form can
be found here.</a> Read on for more information about the different posts.
Unfortunately they're only available in Swedish.

</p>

</div>
-->

Have a question? Don't hesitate to contact us!
You can reach us on [Discord](https://discord.gg/UG5YYsN).

We have meetups **every** Tuesday in Café Java at 17.15. Swing by for some free fika!

# Board

<div id="card-container">

<div class="profile-card">
    <img src="/static/img/board-24-25/morgan.jpg" alt="Morgan">
    <h2>Morgan Nordberg</h2>
    <p class="profile-card-sub">Chairman</p>
    <p class="profile-card-sub"><a href="mailto:ordf@lithekod.se">ordf@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/astrid.jpg" alt="Astrid">
    <h2>Astrid Lauenstein</h2>
    <p class="profile-card-sub">Vice Chairman</p>
    <p class="profile-card-sub"><a href="mailto:vice.ordf@lithekod.se">vice.ordf@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/johannes.jpg" alt="Johannes">
    <h2>Johannes Kung</h2>
    <p class="profile-card-sub">Treasurer</p>
    <p class="profile-card-sub"><a href="mailto:kassor@lithekod.se">kassor@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/kacper.jpg" alt="Kacper">
    <h2>Kacper Uminski</h2>
    <p class="profile-card-sub">Operations Manager</p>
    <p class="profile-card-sub"><a href="mailto:verks@lithekod.se">verks@lithekod.se</a></p>
</div>

<div class="profile-card">
    <img src="/static/img/board-24-25/martin.jpg" alt="Martin">
    <h2>Martin Högstedt</h2>
    <p class="profile-card-sub">Game Jam</p>
    <p class="profile-card-sub"><a href="mailto:gamejam@lithekod.se">gamejam@lithekod.se</a></p>
</div>

</div>

## Other

<div id="card-container">

<div class="profile-card">
    <img src="/static/img/board-22-23/gustav.png" alt="Gustav">
    <h2>Gustav Sörnäs</h2>
    <p class="profile-card-sub">Accountant</p>
    <p class="profile-card-sub"><a href="mailto:revisor@lithekod.se">revisor@lithekod.se</a></p>
</div>

</div>

Gustav's profile image drawn by Annie Wång! Thanks :D
