# Tävlingar

LiTHe kod håller i en hel del programmeringstävlingar under året.
Nedan finner du information om de olika event som vi har.

<!--[mer stycke här, hur man kan engagera sig och lära sig mer kanske?]-->

## Nuvarande tävlingar

- [IMPA](impa/) - Året runt!
- [NCPC](ncpc/2024/)

## Kommande tävlingar

- December 2024: [Advent of Code](aoc/)

## Tidigare tävlingar

### 2023

- Advent of Code
- [NCPC](ncpc/2023/)
- [LiU Challenge (Swedish Coding Cup)](liu-challenge/)

### 2022

- Advent of Code
- [NCPC](ncpc/2022/)

### 2021

- Advent of Code
- [NCPC](ncpc/2021/)

### 2020

- Advent of Code
- [NCPC](ncpc/2020/)

### 2019

- Advent of Code
- [NCPC](ncpc/2019/)
- LiU Challenge (Swedish Coding Cup)

Och säkert några till som vi glömt bort med åren.
